from fastapi import FastAPI, UploadFile, File, HTTPException
from typing import List
import os
import shutil

app = FastAPI()

@app.post("/create-file/{file_path:path}")
async def create_file(file_path: str, file: UploadFile = File(...)):
    os.makedirs(os.path.dirname(file_path), exist_ok=True)
    with open(file_path, "wb") as buffer:
        shutil.copyfileobj(file.file, buffer)
    return {"file_path": file_path}

@app.get("/retrieve-file/{file_path:path}")
async def retrieve_file(file_path: str):
    if not os.path.exists(file_path):
        raise HTTPException(status_code=404, detail="File not found")
    return {"file": file_path}

@app.get("/list-dir/")
async def list_dir(path: str = '.'):
    if not os.path.isdir(path):
        raise HTTPException(status_code=404, detail="Directory not found")
    return os.listdir(path)

@app.get("/navigate-dir/{dir_path:path}")
async def navigate_dir(dir_path: str):
    if not os.path.isdir(dir_path):
        raise HTTPException(status_code=404, detail="Directory not found")
    return {"current_dir": dir_path, "contents": os.listdir(dir_path)}
