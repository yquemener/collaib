# Use an official Python runtime as a parent image
FROM python:3.9

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy the requirements file into the container at /usr/src/app
COPY requirements.txt ./

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Clone the repository from GitLab
RUN apt-get update && apt-get install -y git
RUN git clone https://gitlab.com/yquemener/collaib.git

# Set the working directory to the cloned repository
WORKDIR /usr/src/app/collaib

# Make port 3801 available to the world outside this container
EXPOSE 3801

# Define environment variable
ENV NAME collaib

# Run the application
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "3801"]
